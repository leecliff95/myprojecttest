#pragma warning(disable: 4996)

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<memory.h>
#include<string.h>

#define MAX2(a, b) 	((a) > (b) ? (a) : (b))
#define MIN2(a, b) 	((a) < (b) ? (a) : (b))
#define ABS(x) 		((x) < 0 ? -(x) : (x))

int main(){
	int px, py;
	int qx, qy;
	int dx, dy;

	scanf("%d %d", &px, &py);
	scanf("%d %d", &qx, &qy);
	scanf("%d %d", &dx, &dy);

	/****** Your Code ********/
    if (px==py && px==qx && qx==qy)
        printf("DOT\n");
    else if (px==qx || py==qy)
        printf("SEGMENT\n");
    else if (px==py && px!=qx && qx==qy)
        printf("SQUARE\n");
    else
        printf("RECTANGLE\n");



	/*************************/

	// printf("SEGMENT\n");
	// printf("DOT\n");
	// printf("SQUARE\n");
	// printf("RECTANGLE\n");
	// printf("SEGMENT\n");
	// printf("DOT\n");
	
	return 0;
}